import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { UserContext } from "../context/UserContext";
import { useHistory } from "react-router-dom";

function ElevationScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function Header(props) {
  const classes = useStyles();
  const [user, setUser] = useContext(UserContext)
  const handleLogout = () =>{
    setUser(null)
    localStorage.removeItem("user")
  }
  const history = useHistory();
  const handleClick = (route) => {
    switch (route) {
      case '':
        history.push('/')
        break;
      case 'about':
        history.push('/about')
        break;
      case 'movies':
        history.push('/movies')
        break;
      case 'games':
        history.push('/games')
        break;
      case 'register':
        history.push('/register')
        break;
      case 'login':
        history.push('/login')
        break;
      case 'change-password':
        history.push('/change-password')
        break;
      case 'logout':
        handleLogout();
        break;
    
      default:
        break;
    }
  }
  
  return (
    <React.Fragment>
      <CssBaseline />
      <ElevationScroll {...props}>
        <AppBar>
          <Toolbar>
          <Typography variant="h6" className={classes.title}>
            
          <img id="logo" src="/img/logo.png" width="200px" alt="logo-sanbercode" />
          </Typography>
          <Button color="inherit" onClick={e => handleClick('')}>Home</Button>
          <Button color="inherit" onClick={e => handleClick('about')}>About</Button>
          { user && <Button color="inherit" onClick={e => handleClick('movies')}>Movie List</Button>}
          { user && <Button color="inherit" onClick={e => handleClick('games')}>Game List</Button>}
          { user && <Button color="inherit" onClick={e => handleClick('change-password')}>Change Password</Button>}
          { user === null && <Button color="inherit" onClick={e => handleClick('login')}>Login</Button>}
          { user && <Button color="inherit" onClick={e => handleClick('logout')}>Logout</Button>}
          <Button color="inherit" onClick={e => handleClick('register')}>Register</Button>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />
    </React.Fragment>
  );
}

// export default function ButtonAppBar() {
//   const classes = useStyles();
//   const [user, setUser] = useContext(UserContext);
// const history = useHistory();
//   const handleLogout = () =>{
//     setUser(null)
//     localStorage.removeItem("user");
//     history.push('/')
//   }
// const handleClick = (route) => {
//   switch (route) {
//     case '':
//       history.push('/')
//       break;
//     case 'about':
//       history.push('/about')
//       break;
//     case 'movies':
//       history.push('/movies')
//       break;
//     case 'games':
//       history.push('/games')
//       break;
//     case 'register':
//       history.push('/register')
//       break;
//     case 'login':
//       history.push('/login')
//       break;
//     case 'change-password':
//       history.push('/change-password')
//       break;
//     case 'logout':
//       handleLogout();
//       break;
  
//     default:
//       break;
//   }
// }
//   return (
//     <div className={classes.root}>
//       <AppBar position="static">
//         <Toolbar>
//           <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
//             <MenuIcon />
//           </IconButton>
//           <Typography variant="h6" className={classes.title}>
//             News
//           </Typography>
//           <Button color="inherit" onClick={e => handleClick('')}>Home</Button>
//           <Button color="inherit" onClick={e => handleClick('about')}>About</Button>
//           { user && <Button color="inherit" onClick={e => handleClick('movies')}>Movie List</Button>}
//           { user && <Button color="inherit" onClick={e => handleClick('games')}>Game List</Button>}
//           { user && <Button color="inherit" onClick={e => handleClick('change-password')}>Change Password</Button>}
//           { user === null && <Button color="inherit" onClick={e => handleClick('login')}>Login</Button>}
//           { user && <Button color="inherit" onClick={e => handleClick('logout')}>Logout</Button>}
//           <Button color="inherit" onClick={e => handleClick('register')}>Register</Button>
          
          
          
//         </Toolbar>
//       </AppBar>
//     </div>
//   );
// }