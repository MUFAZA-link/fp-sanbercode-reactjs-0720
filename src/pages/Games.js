import React, {useState, useEffect} from "react"
import axios from "axios"
import "./Games.css"

const Games = () => {
  
  const [games, setGames] =  useState(null)
  const [input, setInput]  =  useState({
    name: "",
    image_url: "",
    genre: "",
    platform: "",
    release: "",
    multiplayer: "",
    singleplayer: "",
  })
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")

  useEffect( () => {
    if (games === null){
      axios.get(`https://www.backendexample.sanbersy.com/api/games`)
      .then(res => {
          console.log('response game',res.data);
          setGames(res.data.map(el=>{ return {
            id: el.id, 
            image_url: el.image_url, 
            name: el.name,
            genre: el.genre,
            platform: el.platform,
            release: el.release,
            multiplayer: el.multiplayer,
            singleplayer: el.singlePlayer,
          }
        }))
      })
    }
  }, [games])
  
  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "name":
      {
        setInput({...input, name: event.target.value});
        break
      }
      case "image_url":
      {
        setInput({...input, image_url: event.target.value});
        break
      }
      case "genre":
      {
        setInput({...input, genre: event.target.value});
        break
      }
      case "release":
      {
        setInput({...input, release: event.target.value});
          break
      }
      case "platform":
      {
        setInput({...input, platform: event.target.value});
          break
      }
      case "multiplayer":
        {
          setInput({...input, multiplayer: parseInt(event.target.value)});
            break
        }
      case "singleplayer":
        {
          setInput({...input, singleplayer: parseInt(event.target.value)});
            break
        }
      
    default:
      {break;}
    }
  }

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let name = input.name
    console.log(input)

    if (name.replace(/\s/g,'') !== ""){      
      if (statusForm === "create"){        
        axios.post(`https://www.backendexample.sanbersy.com/api/games`, {
          image_url: input.image_url, 
          name: input.name,
          genre: input.genre,
          platform: input.platform,
          release: input.release,
          multiplayer: parseInt(input.multiplayer),
          singlePlayer: parseInt(input.singleplayer),
        })
        .then(res => {
          console.log('response create game',res.data);
            setGames([...games, {id: res.data.id, ...input}])
        })
      }else if(statusForm === "edit"){
        axios.put(`https://www.backendexample.sanbersy.com/api/games/${selectedId}`, {
          image_url: input.image_url, 
          name: input.name,
          genre: input.genre,
          platform: input.platform,
          release: input.release,
          multiplayer: parseInt(input.multiplayer),
          singleplayer: parseInt(input.singleplayer),
        })
        .then(res => {
            let singleGame = games.find(el=> el.id === selectedId)
            singleGame.name = input.name
            singleGame.image_url = input.image_url
            singleGame.genre = input.genre
            singleGame.platform = input.platform
            singleGame.multiplayer = input.multiplayer
            singleGame.singleplayer = input.singleplayer
            setGames([...games])
        })
      }
      
      setStatusForm("create")
      setSelectedId(0)
      setInput({
        name: "",
        image_url: "",
        genre: "",
        platform: "",
        release: "",
        multiplayer: "",
        singleplayer: "",
      })
    }

  }

  const Action = ({itemId}) =>{
    const handleDelete = () => {  
      let newGames = games.filter(el => el.id !== itemId)
  
      axios.delete(`https://www.backendexample.sanbersy.com/api/games/${itemId}`)
      .then(res => {
        console.log(res)
      })
            
      setGames([...newGames])
      
    }
    
    const handleEdit = () =>{
      let singleGame = games.find(x=> x.id === itemId)
      setInput({
        image_url: singleGame.image_url, 
        name: singleGame.name,
        genre: singleGame.genre,
        platform: singleGame.platform,
        release: singleGame.release,
        multiplayer: parseInt(singleGame.multiplayer),
        singleplayer: parseInt(singleGame.singleplayer),
      })
      setSelectedId(itemId)
      setStatusForm("edit")
    }

    return(
      <>
        <button onClick={handleEdit}>Edit</button>
        &nbsp;
        <button onClick={handleDelete}>Delete</button>
      </>
    )
  }

  return(
    <>
      <h1>Daftar Game</h1>
      <table>
        <thead>
          <tr>
            {/* <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Year</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Action</th> */}
            <th>No</th>
            <th>Name</th>
            <th>Genre</th>
            <th>Release</th>
            <th>Platform</th>
            <th>Multi Player</th>
            <th>Single Player</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

            {
              games !== null && games.map((item, index)=>{
                return(                    
                  <tr key={index}>
                    {/* <td>{index+1}</td>
                    <td>{item.title}</td>
                    <td>{item.description}</td>
                    <td>{item.year}</td>
                    <td>{item.duration}</td>
                    <td>{item.genre}</td>
                    <td>{item.rating}</td>
                    <td>
                      <Action itemId={item.id} />

                    </td> */}
                    <td>{index+1}</td>
                    <td>{item.name}</td>
                    <td>{item.genre}</td>
                    <td>{item.release}</td>
                    <td>{item.platform}</td>
                    <td>{item.multiplayer === 1 ? 'Yes' : 'No'}</td>
                    <td>{item.singleplayer === 1 ? 'Yes' : 'No'}</td>
                    <td>
                      <Action itemId={item.id} />

                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>
      {/* Form */}
      <h1>Games Form</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label style={{float: "left"}}>
            Name:
          </label>
          <input style={{float: "right"}} type="text" name="name" value={input.name} onChange={handleChange}/>
          <br/>
          <br/>
        </div>
        <div>
          <label style={{float: "left"}}>
            Genre:
          </label>
          <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
          <br/>
          <br/>
        </div>
        <div style={{marginTop: "20px"}}>
          <label style={{float: "left"}}>
            Release:
          </label>
          <input style={{float: "right"}} type="number" name="release" value={input.release} onChange={handleChange}/>
          <br/>
          <br/>
        </div>
        <div style={{marginTop: "20px"}}>
          <label style={{float: "left"}}>
            Platform:
          </label>
          <input style={{float: "right"}} type="text" name="platform" value={input.platform} onChange={handleChange}/>
          <br/>
          <br/>
        </div>
        <div style={{marginTop: "20px"}}>
          <label style={{float: "left"}}>
            Multiplayer:
          </label>
          <label style={{float: "right"}}>
            No
            <input style={{float: "right"}} type="radio" name="multiplayer" checked={parseInt(input.multiplayer) === 0} value="0" onChange={handleChange}/>
          </label>
          <label style={{float: "right"}}>
            Yes
            <input style={{float: "right"}} type="radio" name="multiplayer" checked={parseInt(input.multiplayer) === 1} value="1" onChange={handleChange}/>
          </label>
          <br/>
          <br/>
        </div>
        <div style={{marginTop: "20px"}}>
          <label style={{float: "left"}}>
            Singleplayer:
          </label>
          <label style={{float: "right"}}>
            No
            <input style={{float: "right"}} type="radio" name="singleplayer" checked={parseInt(input.singleplayer) === 0} value="0" onChange={handleChange}/>
          </label>
          <label style={{float: "right"}}>
            Yes
            <input style={{float: "right"}} type="radio" name="singleplayer" checked={parseInt(input.singleplayer) === 1} value="1" onChange={handleChange}/>
          </label>
          <br/>
          <br/>
        </div>
        <div style={{marginTop: "20px"}}>
          <label style={{float: "left"}}>
            Image URL:
          </label>
          <textarea style={{float: "right"}} type="text" name="image_url" value={input.image_url} onChange={handleChange}/>
          <br/>
          <br/>
        </div>
        <button>submit</button>
      </form>
    </>
  )
}

export default Games