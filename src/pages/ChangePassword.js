import React, { useContext, useState, useEffect } from "react"
import {UserContext} from "../context/UserContext"
import {Button} from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import axios from "axios";

const Register = () =>{
  const [user, ] = useContext(UserContext)
  const [input, setInput] = useState({username: "" , password: ""})
  useEffect( () => {
    if (user) {
      setInput({username: user.username,password: ''})
    }
    
  },[user])

  const handleSubmit = (event) =>{
    event.preventDefault()
    if (input.username !== '' && input.password !== '') {
        axios.put(`https://backendexample.sanbersy.com/api/users/${user.id}`,{
      username: input.username,
      password: input.password
      })
      .then(res => {
        console.log('response users',res.data);
        if (typeof res.data === 'object') {
          alert("Berhasil Ganti Password");
        setInput({...input,password: ''})
        }else{
          alert("username dan password salah");
        setInput({username: '',password: ''})
        }
      })
    }else{
      alert("username dan password tidak boleh kosong")
    }
  }

  const handleChange = (event) =>{
    let value = event.target.value;
    let name = event.target.name;
    console.log(`${value} , ${name}`);
    switch (name){
      case "username":{
        setInput({...input, username: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }

  return(
    <>
      <form onSubmit={handleSubmit}>
      <TextField id="username" name="username" disabled={true} label="Username" variant="outlined" onChange={handleChange} value={input.username} />
        <br/>
        <TextField id="password" name="password" type="password" label="Password" variant="outlined" onChange={handleChange} value={input.password} />
        <br/>
        <Button type="submit" variant="contained" color="primary">
        Change Password
      </Button>
      </form>
    </>
  )
}

export default Register