import React from "react"

const About = () =>{
  return (
    <div style={{padding: "10px", border: "1px solid #ccc"}}>
      <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <ol>
        <li><strong style={{width: "100px"}}>Nama:</strong> Muhammad Fauzan Azim</li> 
        <li><strong style={{width: "100px"}}>Email:</strong> muhfauzanazim@gmail.com</li> 
        <li><strong style={{width: "100px"}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
        <li><strong style={{width: "100px"}}>Akun Gitlab:</strong> MUFAZA-link</li> 
        <li><strong style={{width: "100px"}}>Akun Telegram:</strong> @mufaza15</li> 
      </ol>
    </div>
  )
}

export default About