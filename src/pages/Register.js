import React, { useState, useEffect } from "react"
import {Button} from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import axios from "axios";

const Register = () =>{
  const [input, setInput] = useState({username: "" , password: ""})
  const [users, setUsers] =  useState(null)
  useEffect( () => {
    if (users === null){
      axios.get(`https://backendexample.sanbersy.com/api/users`)
      .then(res => {
          setUsers(res.data.map(el=>{ return {
            id: el.id,
            username: el.username,
          }
        }))
        console.log('response users',res.data);
      })
    }
  }, [users])

  const handleSubmit = (event) =>{
    event.preventDefault()
    if (input.username !== '' && input.password !== '') {
      const isUserExists = users.some(el => el.username === input.username);
      if (isUserExists) {
        alert("username telah tersedia");
        setInput({username: '',password: ''})
      } else {
        axios.post(`https://backendexample.sanbersy.com/api/users`,{
      username: input.username,
      password: input.password
      })
      .then(res => {
        console.log('response users',res.data);
        if (typeof res.data === 'object') {
          alert("Berhasil Register");
        setInput({username: '',password: ''})
        axios.get(`https://backendexample.sanbersy.com/api/users`)
        .then(res => {
            setUsers(res.data.map(el=>{ return {
              id: el.id,
              username: el.username,
            }
          }))
          console.log('response users',res.data);
        })
        }else{
          alert("username dan password salah");
        setInput({username: '',password: ''})
        }
      })
      }
      
    }else{
      alert("username dan password tidak boleh kosong")
    }
  }

  const handleChange = (event) =>{
    let value = event.target.value;
    let name = event.target.name;
    console.log(`${value} , ${name}`);
    switch (name){
      case "username":{
        setInput({...input, username: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }

  return(
    <>
      <form onSubmit={handleSubmit}>
      <TextField id="username" name="username" label="Username" variant="outlined" onChange={handleChange} value={input.username} />
        <br/>
        <TextField id="password" name="password" type="password" label="Password" variant="outlined" onChange={handleChange} value={input.password} />
        <br/>
        <Button type="submit" variant="contained" color="primary">
        Register
      </Button>
      </form>
    </>
  )
}

export default Register