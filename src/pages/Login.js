import React, { useContext, useState } from "react"
import {UserContext} from "../context/UserContext"
import {Button} from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import axios from "axios";

const Login = () =>{
  const [, setUser] = useContext(UserContext)
  const [input, setInput] = useState({username: "" , password: ""})

  const handleSubmit = (event) =>{
    event.preventDefault()
    if (input.username !== '' && input.password !== '') {
      axios.post(`https://backendexample.sanbersy.com/api/login`,{
      username: input.username,
      password: input.password
    })
      .then(res => {
        console.log('response users',res.data);
        if (typeof res.data === 'object') {
          setUser({id: res.data.id,
            username: res.data.username, 
            password: res.data.password})
        localStorage.setItem("user", JSON.stringify({
          id: res.data.id,
          username: res.data.username, 
          password: res.data.password
        }))
        }else{
          alert("username dan password salah");
        setInput({username: '',password: ''})
        }
      })
    }else{
      alert("username dan password tidak boleh kosong")
    }
  }

  const handleChange = (event) =>{
    let value = event.target.value;
    let name = event.target.name;
    console.log(`${value} , ${name}`);
    switch (name){
      case "username":{
        setInput({...input, username: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }

  return(
    <>
      <form onSubmit={handleSubmit}>
      <TextField id="username" name="username" label="Username" variant="outlined" onChange={handleChange} value={input.username} />
        <br/>
        <TextField id="password" name="password" type="password" label="Password" variant="outlined" onChange={handleChange} value={input.password} />
        <br/>
        
        <br/>
        <Button type="submit" variant="contained" color="primary">
        Login
        </Button>
      </form>
    </>
  )
}

export default Login